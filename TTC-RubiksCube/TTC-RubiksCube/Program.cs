﻿using System;
using RubiksCubeLibrary;
using Cube;

namespace ConsoleAppRubiksCubeSim
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Console.WriteLine("Welcome to my Rubik's Cube Simulator");

            var rubiksCube = new CubeObject();
            DisplayHandler.PrintEntireCube(rubiksCube);
            Console.WriteLine("\n\nPress any key to begin playing with the cube");
            Console.ReadKey();
            Console.Clear();

            AppCLI.InitiateSimulationLoop(rubiksCube);
                
            Console.ReadKey();
        }
    }
}