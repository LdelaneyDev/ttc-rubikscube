﻿using Cube;
using RubiksCubeLibrary;

namespace ConsoleAppRubiksCubeSim
{
    internal class AppCLI
    {
        /// <summary>
        /// Method provides the loop of simulation logic such as taking in user input and outputting data to the console
        /// </summary>
        /// <param name="rubiksCube"></param>
        public static void InitiateSimulationLoop(CubeObject rubiksCube)
        {
            InputHandler.InitaliseInputDictionary();
            bool solved = false;
            string input;
            Console.WriteLine("Press Enter to begin!");

            // Simulator will loop until cube returns to it's solved state
            while (!solved)
            {
                Console.Clear();
                DisplayHandler.PrintEntireCube(rubiksCube);

                // Print to console our valid inputs and make the user aware of how to stop the sim
                Console.WriteLine($"\n\nInsert movement: F  -  R  -  U  -  B  -  L  -  D" +
                                  $"\n                 F' -  R' -  U' -  B' -  L' -  D'" +
                                  $"\n\nEnter 'stop' to end the simulation.\n\n");

                // Check for 'stop' to return early if possible
                input = Console.ReadLine().ToUpper();
                if (input.ToLower() == "stop")
                    break;

                // Assign user input, apply validation and loop until it matches a value in the Input Dictionary
                input = InputHandler.InputLoop(input);
                if (input.ToLower() == "stop")
                    break;

                Console.WriteLine($"Performing rotation: {input}");
                rubiksCube.ApplyRotation(input);
                solved = rubiksCube.IsSolved();
            }

            Console.Clear();
            DisplayHandler.PrintEntireCube(rubiksCube);
            string messageContent = solved ? $"\n\nCongratulations for solving the Rubik's Cube!" : "\n\n";
            Console.WriteLine($"{messageContent}" +
                              $"\nThank you for playing my Rubik's Cube simulator!" + 
                              $"\nPress Any Key to exit.");
        }
    }
}
