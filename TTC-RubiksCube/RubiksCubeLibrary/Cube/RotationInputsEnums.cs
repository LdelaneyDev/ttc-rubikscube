﻿public enum RotationInputs
{
    FrontFaceClockwise,
    FrontFaceInverse,
    BackFaceClockwise,
    BackFaceInverse,
    TopFaceClockwise,
    TopFaceInverse,
    RightFaceClockwise,
    RightFaceInverse,
    LeftFaceClockwise,
    LeftFaceInverse,
    BottomFaceClockwise,
    BottomFaceInverse,
}