﻿using System.Drawing;

namespace RubiksCubeLibrary.Cube.CubeFace
{
    public class Face
    {
        char[,] faceSquares = new char[3,3];

        /// <summary>
        /// Initialise this object based off the colour supplied by the Cube class
        /// </summary>
        /// <param name="colour"> W, O, G, R, B, Y </param>
        public Face(char colour) 
        {
            SetFaceColour(colour);
        }

        /// <summary>
        /// Provides logic to iterate over our 3x3 Char array and set each 'square' to the colour passed through
        /// </summary>
        /// <param name="colour"> W, O, G, R, B, Y </param>
        public void SetFaceColour(char colour)
        {
            // Loops over each column and sets the adjacent row to the input colour
            for (int col = 0; col < faceSquares.GetLength(0); col++)
                for (int row = 0; row < faceSquares.GetLength(1); row++)
                    faceSquares[col, row] = colour;
        }

        /// <summary>
        /// Checks array for any colour that does not match the first, if found then return false
        /// </summary>
        public bool IsSingleColour()
        {
            var firstColour = faceSquares[0,0];

            for (int col = 0; col < faceSquares.GetLength(0); col++)
                for (int row = 0; row < faceSquares.GetLength(1); row++)
                    if (faceSquares[col, row] != firstColour)
                        return false;

            return true;
        }

        /// <summary>
        /// Provides logic to iterate over our Face and store the anti-clockwise 
        /// rotated face in a var to later re-assign to our face
        /// </summary>
        public void RotateFaceInverse()
        {
            char[,] rotatedFace = new char[3, 3];

            // Iterate over each column, per column iterate over every row, assign the char value in rotated face
            // - 2 from the column when assigning to the rotated face to perform an anti-clockwise rotation
            for (int col = 0; col < 3; col++)
                for (int row = 0; row < 3; row++)
                    rotatedFace[row, 2 - col] = faceSquares[col, row];

            faceSquares = rotatedFace;
        }

        /// <summary>
        /// Provides logic to iterate over our Face and store the clockwise 
        /// rotated face in a var to later re-assign to our face
        /// </summary>
        public void RotateFaceClockwise()
        {
            char[,] rotatedFace = new char[3, 3];

            // Iterate over each column, per column iterate over every row, assign the char value in rotated face
            // - 2 from the row when assigning to the rotated face to perform an clockwise rotation
            for (int col = 0; col < 3; col++)
                for (int row = 0; row < 3; row++)
                    rotatedFace[2 - row, col] = faceSquares[col, row];

            faceSquares = rotatedFace;
        }

        /// <summary>
        /// Method to return our objects faceSqaures property
        /// </summary>
        public char[,] GetFace()
        {
            return faceSquares;
        }

        /// <summary>
        /// Method to return the values which represent the top row of our Cube's Face
        /// </summary>
        public char[] GetTopRow()
        {
            char[] topRow = new char[faceSquares.GetLength(0)];
            
            for (int col = 0; col < topRow.Length; col++)
                topRow[col] = faceSquares[col, 0];

            return topRow;
        }

        /// <summary>
        /// Method to return the values which represent the bottom row of our Cube's Face
        /// </summary>
        public char[] GetBottomRow()
        {
            char[] bottomRow = new char[faceSquares.GetLength(0)];

            for (int col = 0; col < bottomRow.Length; col++)
                bottomRow[col] = faceSquares[col, 2];

            return bottomRow;
        }

        /// <summary>
        /// Method to return the values which represent the left row of our Cube's Face
        /// </summary>
        public char[] GetLeftRow()
        {
            char[] leftRow = new char[faceSquares.GetLength(1)];

            for (int row = 0; row < leftRow.Length; row++)
                leftRow[row] = faceSquares[0, row];

            return leftRow;
        }

        /// <summary>
        /// Method to return the values which represent the right row of our Cube's Face
        /// </summary>
        public char[] GetRightRow()
        {
            char[] rightRow = new char[faceSquares.GetLength(1)];

            for (int row = 0; row < rightRow.Length; row++)
                rightRow[row] = faceSquares[2, row];

            return rightRow;
        }

        public void SetTopRowClockwise(char[] rowToCopy)
        {
            Array.Reverse(rowToCopy);
            for (int col = 0; col < rowToCopy.Length; col++)
                faceSquares[col, 0] = rowToCopy[col];
        }

        public void SetTopRowInverse(char[] rowToCopy)
        {
            for (int col = 0; col < rowToCopy.Length; col++)
                faceSquares[col, 0] = rowToCopy[col];
        }

        public void SetBottomRowClockwise(char[] rowToCopy)
        {
            Array.Reverse(rowToCopy);
            for (int col = 0; col < rowToCopy.Length; col++)
                faceSquares[col, 2] = rowToCopy[col];
        }

        public void SetBottomRowInverse(char[] rowToCopy)
        {
            for (int col = 0; col < rowToCopy.Length; col++)
                faceSquares[col, 2] = rowToCopy[col];
        }

        public void SetLeftRowClockwise(char[] rowToCopy)
        {
            Array.Reverse(rowToCopy);
            for (int row = 0; row < rowToCopy.Length; row++)
                faceSquares[0, row] = rowToCopy[row];
        }

        public void SetLeftRowInverse(char[] rowToCopy)
        {
            for (int row = 0; row < rowToCopy.Length; row++)
                faceSquares[0, row] = rowToCopy[row];
        }

        public void SetRightRowClockwise(char[] rowToCopy)
        {
            Array.Reverse(rowToCopy);
            for (int row = 0; row < rowToCopy.Length; row++)
                faceSquares[2, row] = rowToCopy[row];
        }

        public void SetRightRowInverse(char[] rowToCopy)
        {
            for (int row = 0; row < rowToCopy.Length; row++)
                faceSquares[2, row] = rowToCopy[row];
        }
    }
}
