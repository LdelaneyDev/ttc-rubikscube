﻿public enum ColourEnums
{
    White  = 'W',
    Orange = 'O',
    Green  = 'G',
    Red    = 'R',
    Blue   = 'B',
    Yellow = 'Y',
}