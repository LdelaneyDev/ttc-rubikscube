﻿using RubiksCubeLibrary;
using RubiksCubeLibrary.Cube.CubeFace;

namespace Cube
{
    public class CubeObject
    {  
        public Face Front { get; private set; }
        public Face Back { get; private set; }
        public Face Top { get; private set; }
        public Face Bottom { get; private set; }
        public Face Left { get; private set; }
        public Face Right { get; private set; }

        /// <summary>
        /// Initalise Cube as per Rubiks-Cube-Solver.com's default configuration
        /// </summary>
        public CubeObject()
        {
            Front  = new Face((char)ColourEnums.Green);
            Back   = new Face((char)ColourEnums.Blue);
            Top    = new Face((char)ColourEnums.White);
            Bottom = new Face((char)ColourEnums.Yellow);
            Left   = new Face((char)ColourEnums.Orange);
            Right  = new Face((char)ColourEnums.Red);
        }

        /// <summary>
        /// Using the input provided by the user, route the program to the 
        /// appropriate function to perform the logic of the move
        /// </summary>
        /// <param name="input"> User Input which matches a value in the Valid Inputs Dictionary </param>
        public void ApplyRotation(string input)
        {
            RotationInputs inputValue = InputHandler.ValidInputsDictionary[input];

            switch (inputValue)
            {
                case RotationInputs.FrontFaceClockwise:
                    RotateFrontFaceClockwise();
                    break;
                case RotationInputs.FrontFaceInverse:
                    RotateFrontFaceInverse();
                    break;
                case RotationInputs.BackFaceClockwise:
                    RotateBackFaceClockwise();
                    break;
                case RotationInputs.BackFaceInverse:
                    RotateBackFaceInverse();
                    break;
                case RotationInputs.TopFaceClockwise:
                    RotateTopFaceClockwise();
                    break;
                case RotationInputs.TopFaceInverse:
                    RotateTopFaceInverse();
                    break;
                case RotationInputs.RightFaceClockwise:
                    RotateRightFaceClockwise();
                    break;
                case RotationInputs.RightFaceInverse:
                    RotateRightFaceInverse();
                    break;
                case RotationInputs.LeftFaceClockwise:
                    RotateLeftFaceClockwise();
                    break;
                case RotationInputs.LeftFaceInverse:
                    RotateLeftFaceInverse();
                    break;
                case RotationInputs.BottomFaceClockwise:
                    RotateBottomFaceClockwise();
                    break;
                case RotationInputs.BottomFaceInverse:
                    RotateBottomFaceInverse();
                    break;
            }
        }

        /// <summary>
        /// Checks all faces to see if they consist of a single colour, if so the cube is solved
        /// </summary>
        public bool IsSolved()
        {
            var isSolved = false;

            if (Top.IsSingleColour() &&
                Bottom.IsSingleColour() &&
                Left.IsSingleColour() &&
                Right.IsSingleColour() &&
                Front.IsSingleColour() &&
                Back.IsSingleColour())
                isSolved = true;

            return isSolved;
        }

        // Code Below Mimics the moves of a Rubiks cube, refactor considered but put on hold until client
        // provides more midnight oil as all our team's allocation of midnight oil has been burned

        public void RotateFrontFaceClockwise()
        {
            var TopFaceBottomRow = Top.GetBottomRow();
            Top.SetBottomRowClockwise(Left.GetRightRow());
            Left.SetRightRowInverse(Bottom.GetTopRow());
            Bottom.SetTopRowClockwise(Right.GetLeftRow());
            Right.SetLeftRowInverse(TopFaceBottomRow);
            Front.RotateFaceClockwise();
        }

        public void RotateFrontFaceInverse()
        {
            var LeftFaceRightRow = Left.GetRightRow();
            Left.SetRightRowClockwise(Top.GetBottomRow());
            Top.SetBottomRowInverse(Right.GetLeftRow());
            Right.SetLeftRowClockwise(Bottom.GetTopRow());
            Bottom.SetTopRowInverse(LeftFaceRightRow);
            Front.RotateFaceInverse();
        }

        public void RotateRightFaceClockwise()
        {
            var backFaceLeftRow = Back.GetLeftRow();
            Back.SetLeftRowInverse(Top.GetRightRow());
            Top.SetRightRowInverse(Front.GetRightRow());
            Front.SetRightRowInverse(Bottom.GetRightRow());
            Bottom.SetRightRowClockwise(backFaceLeftRow);
            Right.RotateFaceClockwise();
        }

        public void RotateRightFaceInverse()
        {
            var backFaceLeftRow = Back.GetLeftRow();
            Back.SetLeftRowClockwise(Bottom.GetRightRow());
            Bottom.SetRightRowInverse(Front.GetRightRow());
            Front.SetRightRowInverse(Top.GetRightRow());
            Top.SetRightRowClockwise(backFaceLeftRow);
            Right.RotateFaceInverse();
        }

        public void RotateTopFaceClockwise()
        {
            var leftFaceTopRow = Left.GetTopRow();
            Left.SetTopRowInverse(Front.GetTopRow());
            Front.SetTopRowInverse(Right.GetTopRow());
            Right.SetTopRowInverse(Back.GetTopRow());
            Back.SetTopRowInverse(leftFaceTopRow);
            Top.RotateFaceClockwise();
        }

        public void RotateTopFaceInverse()
        {
            var backFaceTopRow = Back.GetTopRow();
            Back.SetTopRowClockwise(Right.GetTopRow());
            Right.SetTopRowClockwise(Front.GetTopRow());
            Front.SetTopRowClockwise(Left.GetTopRow());
            Left.SetTopRowClockwise(backFaceTopRow);
            Top.RotateFaceInverse();
        }

        public void RotateBackFaceClockwise()
        {
            var TopFaceTopRow = Top.GetTopRow();
            Top.SetTopRowInverse(Right.GetRightRow());
            Right.SetRightRowClockwise(Bottom.GetBottomRow());
            Bottom.SetBottomRowInverse(Left.GetLeftRow());
            Left.SetLeftRowClockwise(TopFaceTopRow);
            Back.RotateFaceClockwise();
        }

        public void RotateBackFaceInverse()
        {
            var TopFaceTopRow = Top.GetTopRow();
            Top.SetTopRowClockwise(Left.GetLeftRow());
            Left.SetLeftRowInverse(Bottom.GetBottomRow());
            Bottom.SetBottomRowClockwise(Right.GetRightRow());
            Right.SetRightRowInverse(TopFaceTopRow);
            Back.RotateFaceInverse();
        }

        public void RotateLeftFaceClockwise()
        {
            var backFaceRightRow = Back.GetRightRow();
            Back.SetRightRowClockwise(Bottom.GetLeftRow());
            Bottom.SetLeftRowInverse(Front.GetLeftRow());
            Front.SetLeftRowInverse(Top.GetLeftRow());
            Top.SetLeftRowClockwise(backFaceRightRow);
            Left.RotateFaceClockwise();
        }

        public void RotateLeftFaceInverse()
        {
            var backFaceRightRow = Back.GetRightRow();
            Back.SetRightRowClockwise(Top.GetLeftRow());
            Top.SetLeftRowInverse(Front.GetLeftRow());
            Front.SetLeftRowInverse(Bottom.GetLeftRow());
            Bottom.SetLeftRowClockwise(backFaceRightRow);
            Left.RotateFaceInverse();
        }

        public void RotateBottomFaceClockwise()
        {
            var leftFaceBottomRow = Left.GetBottomRow();
            Left.SetBottomRowInverse(Back.GetBottomRow());
            Back.SetBottomRowInverse(Right.GetBottomRow());
            Right.SetBottomRowInverse(Front.GetBottomRow());
            Front.SetBottomRowInverse(leftFaceBottomRow);
            Bottom.RotateFaceClockwise();
        }

        public void RotateBottomFaceInverse()
        {
            var leftFaceBottomRow = Left.GetBottomRow();
            Left.SetBottomRowInverse(Front.GetBottomRow());
            Front.SetBottomRowInverse(Right.GetBottomRow());
            Right.SetBottomRowInverse(Back.GetBottomRow());
            Back.SetBottomRowInverse(leftFaceBottomRow);
            Bottom.RotateFaceInverse();
        }
    }
}
