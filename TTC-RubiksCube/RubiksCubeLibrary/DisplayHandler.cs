﻿using Cube;

namespace RubiksCubeLibrary
{
    public static class DisplayHandler
    {
        /// <summary>
        /// Function to iterate over the long row of faces & print the char's in the Face's 2d array to the console
        /// </summary>
        public static void PrintAdjacentFaces(char[,] leftFace, char[,] frontFace, char[,] rightFace, char[,] backFace)
        {
            var totalColumnCount = leftFace.GetLength(0) + frontFace.GetLength(0) + rightFace.GetLength(0) + backFace.GetLength(0);
            var rowCount = leftFace.GetLength(1);

            for (int row = 0; row < rowCount; row++)
            {
                Console.WriteLine("");
                for (int col = 0; col < totalColumnCount; col++)
                {
                    switch (col) {

                        case < 3:
                            Console.Write(leftFace[col, row]);
                            continue;
                        case < 6:
                            Console.Write(frontFace[col - 3, row]);
                            continue;
                        case < 9:
                            Console.Write(rightFace[col - 6, row]);
                            continue;
                        case < 12:
                            Console.Write(backFace[col - 9, row]);
                            continue;
                        default:
                            Console.WriteLine("Out of Bounds");
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Combines all functions within this class to formulate the entirety of the Rubik's Cube
        /// </summary>
        public static void PrintEntireCube(CubeObject rubiksCube)
        {
            PrintSingularPaddedFace(rubiksCube.Top.GetFace());
            PrintAdjacentFaces(rubiksCube.Left.GetFace(),
                               rubiksCube.Front.GetFace(),
                               rubiksCube.Right.GetFace(),
                               rubiksCube.Back.GetFace());
            PrintSingularPaddedFace(rubiksCube.Bottom.GetFace());
        }

        /// <summary>
        /// Function to iterate over the floating face & print the chars in the Face's 2d array to the console
        /// </summary>
        public static void PrintSingularPaddedFace(char[,] face)
        {
            for (int row = 0; row < face.GetLength(1); row++)
            {
                Console.WriteLine("");
                for (int col = 0; col < face.GetLength(0) * 2; col++)
                {
                    switch (col)
                    {
                        case >= 3:
                            Console.Write(face[col - 3, row]);
                            break;
                        default:
                            Console.Write(" ");
                            break;
                    }

                }
            }
        }
    }
}
