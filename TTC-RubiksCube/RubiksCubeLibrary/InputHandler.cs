﻿namespace RubiksCubeLibrary
{
    public static class InputHandler
    {
        public static Dictionary<string, RotationInputs> ValidInputsDictionary { get; private set; }

        /// <summary>
        /// Setup the input dictionary to map the input keys to human readable enums
        /// </summary>
        public static void InitaliseInputDictionary()
        {
            ValidInputsDictionary = new Dictionary<string, RotationInputs>
            {
                { "F",  RotationInputs.FrontFaceClockwise },
                { "F'", RotationInputs.FrontFaceInverse },
                { "R",  RotationInputs.RightFaceClockwise },
                { "R'", RotationInputs.RightFaceInverse },
                { "U",  RotationInputs.TopFaceClockwise },
                { "U'", RotationInputs.TopFaceInverse },
                { "B",  RotationInputs.BackFaceClockwise },
                { "B'", RotationInputs.BackFaceInverse },
                { "L",  RotationInputs.LeftFaceClockwise },
                { "L'", RotationInputs.LeftFaceInverse },
                { "D",  RotationInputs.BottomFaceClockwise },
                { "D'", RotationInputs.BottomFaceInverse }
            };
        }

        /// <summary>
        /// Checks if input is 'stop' to return early if possible, otherwise check the input against
        /// Our input dictionary for a valid match
        /// </summary>
        /// <param name="input"></param>
        public static bool VerifyInputIsValid(string input)
        {
            if (input.ToLower() == "stop")
                return true;

            foreach(KeyValuePair<string, RotationInputs> inputs in ValidInputsDictionary)
            {
                if (inputs.Key == input) { return true; }
            }

            Console.WriteLine("Please enter a valid input...");
            return false;
        }

        /// <summary>
        /// Initiate the loop to read the user input and continue until the user stops or enters a correct input
        /// Added .ToUpper() to the input to minimise human error
        /// </summary>
        /// <param name="input"></param>
        public static string InputLoop(string input)
        {
            while (!VerifyInputIsValid(input))
            {
                input = Console.ReadLine().ToUpper();

                if (input.ToLower() == "stop")
                    break;

                continue;
            }
            return input;
        }
    }
}
