using Cube;
using RubiksCubeLibrary;

namespace RubiksCubeTests
{
    [TestClass]
    public class CubeTests
    {
        //[TestMethod]
        //public void Test_DisplayHandler_PrintEntireCube()
        //{
        //    // Arrange
        //    var rubiksCube = default(CubeObject);

        //    // Act
        //    DisplayHandler.PrintEntireCube(rubiksCube);

        //    // Assert

        //    // TODO add mocking so we can check console outputs

        //}

        [TestMethod]
        public void Test_Face_GetFace()
        {
            // Arrange
            CubeObject rubiksCube = new CubeObject();

            // Act
            var face = rubiksCube.Back.GetFace();

            // Assert
            Assert.IsNotNull(face);
        }

        [TestMethod]
        public void Test_Face_GetTopRow()
        {
            // Arrange
            CubeObject rubiksCube = new CubeObject();

            // Act
            var topRow = rubiksCube.Back.GetTopRow();

            // Assert
            Assert.IsNotNull(topRow);
        }

        [TestMethod]
        public void Test_Face_GetBottomRow()
        {
            // Arrange
            CubeObject rubiksCube = new CubeObject();

            // Act
            var bottomRow = rubiksCube.Back.GetBottomRow();

            // Assert
            Assert.IsNotNull(bottomRow);
        }

        [TestMethod]
        public void Test_Face_GetLeftRow()
        {
            // Arrange
            CubeObject rubiksCube = new CubeObject();

            // Act
            var leftRow = rubiksCube.Back.GetLeftRow();

            // Assert
            Assert.IsNotNull(leftRow);
        }

        [TestMethod]
        public void Test_Face_GetRightRow()
        {
            // Arrange
            CubeObject rubiksCube = new CubeObject();

            // Act
            var rightRow = rubiksCube.Back.GetRightRow();

            // Assert
            Assert.IsNotNull(rightRow);
        }

        [TestMethod]
        public void Test_Face_IsSingleColour()
        {
            // Arrange
            CubeObject rubiksCube = new CubeObject();

            // Act
            var result = rubiksCube.Back.IsSingleColour();

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Test_Cube_IsSolved()
        {
            // Arrange
            CubeObject rubiksCube = new CubeObject();

            // Act
            var result = rubiksCube.IsSolved();

            // Assert
            Assert.IsTrue(result);
        }
    }
}