# TTC-RubiksCube

## Description
This console application is a simulation of a rubik's cube using rubiks-cube-solver.com as a basis for the representation of the cube and moves the user can input.

The application is a simple design divided into 3 projects:

ConsoleAppRubiksCubeSim - This project provides the application main entry point as well as basic UI for the Console Output, as well as the simulation loop logic.

RubiksCubeLibrary       - The purpose of this project is to encapsulate the design and implementation of a Rubik's cube, therefore it contains subclasses for the Cube, Face, Enums, I/O Handling

RubiksCubeTests         - This project contains basic MSTest functionality to provide some basic testing functionality of the application, there is room to develop this to encorporate mocking to expand our test coverage


## Installation
To get started with using this project complete the following steps:
1.  Ideally install a modern IDE with lots of functionality, I would recommend VS2022: https://visualstudio.microsoft.com/vs/
2.  Ensure your system has the .NET6 runtime or sdk installed, download it from here if needed: https://dotnet.microsoft.com/en-us/download/dotnet/6.0
3.  Ensure git is installed on your system, this can be installed via command line or executable. For more information follow this page: https://git-scm.com/downloads
4.  Now using your method of choice, git clone the repo:  https://gitlab.com/LdelaneyDev/ttc-rubikscube.git
5.  In the repo directory, navigate to the TTC-RubiksCube directory and open the TTC-RubiksCube.sln in your IDE
6.  Build the solution
7.  In the debug tab, run all tests to ensure everything has gone smoothly
8.  You are now free to play with the Rubik's Cube and/or inspect or modify the code!

## Usage
Once you have the application running, you will be prompted with a welcome screen displaying the initial configuration of the Rubik's Cube.

When you are ready to begin the simulation, press any key to progress onto the control input screen.

Various inputs are accepted, these are as follow:

	F  -   Rotates The Front Face Clockwise
	
	F' -   Rotates The Front Face Inverse
	
	R  -   Rotates The Right Face Clockwise
	
	R' -   Rotates The Right Face Inverse
	
	U  -   Rotates The Top Face Clockwise
	
	U' -   Rotates The Top Face Inverse
	
	B  -   Rotates The Back Face Clockwise
	
	B' -   Rotates The Back Face Inverse
	
	L  -   Rotates The Left Face Clockwise
	
	L' -   Rotates The Left Face Inverse
	
	D  -   Rotates The Bottom Face Clockwise
	
	D' -   Rotates The Bottom Face Inverse
	
	stop - Ends the simulation
	
	
The simulation will continue until either 'stop' is entered, or the Cube returns to a solved state.

## Roadmap
Future plans for the project include:

	Add a cutting edge front end UI
	
	Expand test coverage to access more void methods, this can be achieved by utilising mocking frameworks.
	
	Refactor methods or explore other avenues for rotation functionality.
	

## Authors and acknowledgment
Liam Delaney

## License
WARNING This is unlincensed software, use at your own risk.

However; I, Liam Delaney, personally approve of the sharing and modification of this software.